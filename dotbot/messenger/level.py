class Level(object):
    NOTSET = 0
    DEBUG = 10
    VERBOSE = 15
    INFO = 20
    WARNING = 30
    ERROR = 40
