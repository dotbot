from .messenger import Messenger
from .context import Context

class Plugin(object):
    '''
    Abstract base class for commands that process directives.
    '''

    _directive = None

    def __init__(self, context):
        self._context = context
        self._log = Messenger()

    def can_handle(self, directive):
        '''
        Returns true if the Plugin can handle the directive.
        '''
        return directive == self._directive

    def handle(self, directive, data, dry_run = False):
        '''
        Executes the directive.

        Returns true if the Plugin successfully handled the directive.

        When dry_run is True, the plugin will not perform any actions, only
        return False when there is nothing to do, True otherwise.
        '''
        raise NotImplementedError
