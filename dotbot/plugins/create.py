import grp
import os
import pwd

import dotbot


class Create(dotbot.Plugin):
    '''
    Create empty paths.
    '''

    _directive = 'create'

    def handle(self, directive, items, dry_run):
        success = True
        for item in items:
            mode  = 0o640
            user  = -1
            group = -1

            if isinstance(item, dict):
                path  = item.get('path')

                val = item.get('mode')
                if isinstance(val, str):
                    mode = int(val, 8)
                elif isinstance(val, int):
                    mode = val
                elif val is not None:
                    self._log.error('mode should be int or str; got %s(%s)' % (str(val), type(val)))
                    success = False
                    continue

                val = item.get('user')
                if isinstance(val, int):
                    user = val
                elif isinstance(val, str):
                    user = pwd.getpwnam(val).pw_uid
                elif val is not None:
                    self._log.error('user should be int or str; got %s(%s)' % (str(val), type(val)))
                    success = False
                    continue

                val = item.get('group')
                if isinstance(val, int):
                    group = val
                elif isinstance(val, str):
                    group = grp.getgrnam(val).gr_gid
                elif val is not None:
                    self._log.error('group should be int or str; got %s(%s)' % (str(val), type(val)))
                    success = False
                    continue
            else:
                path  = item

            path = os.path.expandvars(os.path.expanduser(path))
            success &= self._create(path, dry_run, mode, user, group)

        if dry_run:
            if success:
                self._log.verbose('create/dry run: nothing to do')
            else:
                self._log.verbose('create/dry run: some targets are missing')
        else:
            if success:
                self._log.verbose('All paths have been set up')
            else:
                self._log.error('Some paths were not successfully set up')

        return success

    def _create(self, path, dry_run, mode, user, group):
        if os.path.exists(path):
            self._log.verbose('Path exists %s' % path)
            return True

        success = True
        self._log.info('Creating path %s' % path)
        if dry_run:
            success = False
        else:
            try:
                os.makedirs(path, mode = mode)

                if user >= 0 or group >= 0:
                    try:
                        os.chown(path, user, group)
                    except Exception as e:
                        self._log.error('Error changing ownership of "%s" to %d/%d: %s' %
                                        (path, user, group, str(e)))
                        os.rmdir(path)
                        raise
            except OSError:
                self._log.error('Failed to create path %s' % path)
                success = False

        return success
